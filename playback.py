from flask import Flask, render_template, jsonify, request
from threading import Timer, Thread
from time import sleep
import json
import os

app = Flask(__name__)

from os import listdir
from os.path import isfile, join
current_directory = os.path.dirname(os.path.realpath(__file__))
mypath = current_directory + '/MLB_Test_Data' # Change accordingly
frames = [ f for f in listdir(mypath) if (isfile(join(mypath,f)) and f.endswith( ('.json') ))]
frame_number = 0
current_frame = {}
update_rate = 5

@app.route("/")
def api():
    data = current_frame
    response = jsonify(data)
    response.status_code = 200
    return response

@app.route("/settings")
def settings():
    global frame_number
    data = {
        'frame number': frame_number,
        'update_rate': update_rate
    }
    response = jsonify(data)
    response.status_code = 200
    return response
 
 
class Scheduler(object):
    def __init__(self, sleep_time, function):
        self.sleep_time = sleep_time
        self.function = function
        self._t = None
 
    def start(self):
        if self._t is None:
            self._t = Timer(self.sleep_time, self._run)
            self._t.start()
        else:
            raise Exception("this timer is already running")
 
    def _run(self):
        self.function()
        self._t = Timer(self.sleep_time, self._run)
        self._t.start()
 
    def stop(self):
        if self._t is not None:
            self._t.cancel()
            self._t = None

def query_db():
    t = get_next_frame()
    try:
        temp_json = open(mypath + '/' + frames[t])
    except:
        temp_json = {
            'ERROR' : 'Out of Frames. Try Restarting'
        }
    global current_frame
    current_frame = json.load(temp_json)

def get_next_frame():
    global frame_number
    frame_number = frame_number + 1
    return frame_number
 
if __name__ == "__main__":
    scheduler = Scheduler(update_rate, query_db)
    scheduler.start()
    app.run(host='127.0.0.1', debug=True, port=1337)
    scheduler.stop()